﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="header.aspx.cs" Inherits="GroupProject.Client_Side.header" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <link href="style.css" rel="stylesheet" />
</head>
<body>

    <header>
        <a id="logo" href="index.html"><img src="../Images/logo.jpg" /></a>

        <div id="event_find_cont">
            <form style="float:left;" action="list.html">
                <input id="find_text" type="text" name="search_text" placeholder="Enter event name, artist, date..." />
                <input id="find_submit" type="submit" name="submit_search" value="" />
            </form>
            <!--<div id="search_border"></div>

            <a href="#" id="find_event" role="button">Event Finder</a>-->
        </div>
    </header>
    <div id="main_menu">
        <nav>
            <a id="home" href="index.html">Home</a>
            <div class="wrapper">
                <a id="music" href="#">Music</a>
                <ul id="music_drop" class="menu_dropdown">
                    <li><a href="#">Temp 1</a></li>
                    <li><a href="#">Temp 1</a></li>
                    <li><a href="#">Temp 1</a></li>
                </ul>
            </div>
            <div class="wrapper">
                <a id="sport" href="#">Sport</a>
                <ul id="sport_drop" class="menu_dropdown">
                    <li><a href="#">Temp 1</a></li>
                    <li><a href="#">Temp 1</a></li>
                </ul>
            </div>
            <div class="wrapper">
                <a id="comedy" href="#">Comedy</a>
                <ul id="comedy_drop" class="menu_dropdown">
                    <li><a href="#">Temp 1</a></li>
                    <li><a href="#">Temp 1</a></li>
                    <li><a href="#">Temp 1</a></li>
                    <li><a href="#">Temp 1</a></li>
                </ul>
            </div>
            <div class="wrapper">
                <a id="theatre" href="#">Theatre</a>
                <ul id="theatre_drop" class="menu_dropdown">
                    <li><a href="#">Temp 1</a></li>
                    <li><a href="#">Temp 1</a></li>
                    <li><a href="#">Temp 1</a></li>
                    <li><a href="#">Temp 1</a></li>
                </ul>
            </div>
        </nav>
    </div>
</body>
</html>
