﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Web.Script.Serialization;
using System.Web.Http.Controllers;
using Newtonsoft.Json;

namespace GroupProject.Client_Side
{
    public class APIController : ApiController
    {
        public class Event
        {
            [JsonProperty("EVENTID")]
            double eventID { get; set; }
            [JsonProperty("COMPANYID")]
            double companyID { get; set; }
            [JsonProperty("VENUEID")]
            double venueID { get; set; }
            [JsonProperty("EVENTNAME")]
            public string eventName { get; set; }
            [JsonProperty("EVENTTYPE")]
            string eventType { get; set; }
            [JsonProperty("STARTTIME")]
            string startTime { get; set; }
            [JsonProperty("ENDTIME")]
            string endTime { get; set; }
            [JsonProperty("SEATINGPRICE")]
            string seatingPrice { get; set; }
            [JsonProperty("STANDINGPRICE")]
            string standingPrice { get; set; }
            [JsonProperty("AGERESTRICTION")]
            string ageRestriction { get; set; }
        }
        public class Guests
        {
            public double dateOfBirth { get; set; }
            public double guestID { get; set; }
            public double userID { get; set; }
        }
        public class Venues
        {
            public double venueID { get; set; }
            public double venueName { get; set; }
            public double venueAddressLine1 { get; set; }
            public double venueAddressLine2 { get; set; }
            public double venuepostcode { get; set; }
            public double venueMaxCap { get; set; }
            public double venueSeatCap { get; set; }
            public double venueStandCap { get; set; }
        }
        public class Companies
        {
            public double companyID { get; set; }
            public string companyName { get; set; }
            public string companyEmail { get; set; }
            public double companyPhone { get; set; }
        }
        public class Customers
        {
            public double customerID { get; set; }
            public double userID { get; set; }
            public string customerAddressLine1 { get; set; }
            public string customerAddressLine2 { get; set; }
            public string customerPostcode { get; set; }
            public double customerDateOfBirth { get; set; }
            public string customerPhone { get; set; }
            public string customerPassword { get; set; }
        }
        public class Tickets
        {
            public double eventID { get; set; }
            public double userID { get; set; }
            public double ticketID { get; set; }
            public string ticketType { get; set; }
        }
        public class Users
        {
            public double userID { get; set; }
            public string userFirstName { get; set; }
            public string userLastName { get; set; }
            public string userEmail { get; set; }
        }

        /*protected void Page_Load(object sender, EventArgs e)
        {
            WebRequest request = WebRequest.Create("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251f/api/api/");

            WebResponse response = request.GetResponse();

            Debug.WriteLine(((HttpWebResponse)response).StatusDescription);

            Stream dataStream = response.GetResponseStream();

            StreamReader reader = new StreamReader(dataStream);

            String responseFromServer = reader.ReadToEnd();

            Debug.WriteLine(responseFromServer);

            reader.Close();

            response.Close();
        }*/

        protected void Get_Response(WebRequest request)
        {
            WebResponse response = request.GetResponse();

            Debug.WriteLine(((HttpWebResponse)response).StatusDescription);

            Stream dataStream = response.GetResponseStream();

            StreamReader reader = new StreamReader(dataStream);

            String responseFromServer = reader.ReadToEnd();

            //Get_FormattedData(responseFromServer);

            Debug.WriteLine(responseFromServer);

            reader.Close();

            response.Close();
        }

        protected void Get_FormattedData(String responseFromServer)
        {
            //string[] keyData;
            JavaScriptSerializer parser = new JavaScriptSerializer();

            //ArrayOfCOMPANy?
            if (responseFromServer.Contains("ArrayOfEvent"))
            {
                var data = parser.Deserialize<Event>(responseFromServer);
                data.eventName = "";
            }
            if (responseFromServer.Contains("Guest")){
                var data = parser.Deserialize<Guests>(responseFromServer);
            }
            if (responseFromServer.Contains("Venue")){
                var data = parser.Deserialize<Venues>(responseFromServer);
            }
            if (responseFromServer.Contains("Company")){
                var data = parser.Deserialize<Companies>(responseFromServer);
            }
            if (responseFromServer.Contains("Customer")){
                var data = parser.Deserialize<Customers>(responseFromServer);
            }
            if (responseFromServer.Contains("Ticket")){
                var data = parser.Deserialize<Tickets>(responseFromServer);
            }
            if (responseFromServer.Contains("User"))
            {
                var data = parser.Deserialize<Users>(responseFromServer);
            }
            //data.eventid :P
        }

        public void Call_Events()
        {
            WebRequest request = WebRequest.Create("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251f/api/api/events");
            Get_Response(request);
        }
        protected void Call_Guests()
        {
            WebRequest request = WebRequest.Create("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251f/api/api/guests");
            Get_Response(request);
        }
        protected void Call_Venues()
        {
            WebRequest request = WebRequest.Create("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251f/api/api/venues");
            Get_Response(request);
        }
        protected void Call_Companies()
        {
            WebRequest request = WebRequest.Create("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251f/api/api/companies");
            Get_Response(request);
        }
        protected void Call_Customers()
        {
            WebRequest request = WebRequest.Create("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251f/api/api/customers");
            Get_Response(request);
        }
        protected void Call_Tickets()
        {
            WebRequest request = WebRequest.Create("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251f/api/api/tickets");
            Get_Response(request);
        }
        protected void Call_Users()
        {
            WebRequest request = WebRequest.Create("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251f/api/api/users");
            Get_Response(request);
        }
    }
}
