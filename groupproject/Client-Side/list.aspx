﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="list.aspx.cs" Inherits="GroupProject.Client_Side.list" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <!--datepicker files-->
    <link href="datepicker.css" rel="stylesheet" />
    <script src="../Scripts/jquery.js"></script>
    <script src="../Scripts/datepicker.js" ></script>
    <script src="../Scripts/eye.js"></script>
    <script src="../Scripts/utils.js"></script>
    <script src="../Scripts/layout.js"></script>
    <!-- end -->
    <script src="../Scripts/jquery-2.2.1.min.js" ></script>
    <script type="text/javascript">
        $(function () {
            $("#includedHeader").load("header.aspx");
            $("#includedFooter").load("footer.aspx");
        });
        $('#date').DatePicker({
            flat: true,
            date: '2016-02-31',
            current: '2016-02-31',
            calendars: 1,
            mode: 'range',
            starts: 1
        });
    </script>
</head>
<body>
    <div id="includedHeader"></div>
    <div style="clear: both;"></div>
    <div id="contents">
        <div id="filter_cont">
            <div id="list_filter">
                <p class="header">Filter</p>
                <p class="sub_header">Categories</p>
                <form action="list.html">
                    <ul>
                        <li>
                            <p>
                                <input type="checkbox" name="music" value="music" />Music</p>
                        </li>
                        <li>
                            <p>
                                <input type="checkbox" name="sport" value="sport"/>Sport</p>
                        </li>
                        <li>
                            <p>
                                <input type="checkbox" name="comedy" value="comedy"/>Comedy</p>
                        </li>
                        <li>
                            <p>
                                <input type="checkbox" name="theatre" value="theatre"/>Theatre</p>
                        </li>
                    </ul>
                    <p class="sub_header">Where</p>
                    <input id="where_input" type="Text" name="localpostecode" placeholder="Local Postcode" />
                    <p class="sub_header">Venues</p>
                    <ul>
                        <li>
                            <p>
                                <input type="checkbox" name="venue1" value="venue1"/>Local Venue 1</p>
                        </li>
                        <li>
                            <p>
                                <input type="checkbox" name="venue2" value="venue2"/>Local Venue 2</p>
                        </li>
                        <li>
                            <p>
                                <input type="checkbox" name="venue3" value="venue3"/>Local Venue 3</p>
                        </li>
                        <li>
                            <p>
                                <input type="checkbox" name="venue4" value="venue4"/>Local Venue 4</p>
                        </li>
                    </ul>
                    <div id="date_cont">
                        <p id="date"></p>
                    </div>
                    <input id="filter_submit" type="submit" name="submit_search" value="Search" />
                </form>
            </div>
        </div>
        <div id="list_cont">
            <a href="event_details.html" class="list_item_link"><div class="list_item">
                <!--<a class="event_img" href="event_details.html"</a>>-->
                    <img src="../Images/tmp_img.jpg" />
                <div class="item_text">
                    <p class="header">Justin Bieber World Tour</p>
                    <p>Details</p>
                </div>
                <!--<a href="event_details.html" class="view_event" role="button">View Details</a>-->
                <p class="view_event" role="button">View Details</p>
            </div></a>
            <div class="list_item">
                <a href="event_details.html">
                    <img src="../Images/tmp_img.jpg" /></a>
                <div class="item_text">
                    <p class="header">Justin Bieber World Tour</p>
                    <p>Details</p>
                </div>
                <a href="event_details.html" class="view_event" role="button">View Details</a>
            </div>
            <div class="list_item">
                <a href="event_details.html">
                    <img src="../Images/tmp_img.jpg" /></a>
                <div class="item_text">
                    <p class="header">Justin Bieber World Tour</p>
                    <p>Details</p>
                </div>
                <a href="event_details.html" class="view_event" role="button">View Details</a>
            </div>
            <div class="list_item">
                <a href="event_details.html">
                    <img src="../Images/tmp_img.jpg" /></a>
                <div class="item_text">
                    <p class="header">Justin Bieber World Tour</p>
                    <p>Details</p>
                </div>
                <a href="event_details.html" class="view_event" role="button">View Details</a>
            </div>
        </div>
    </div>
    <div style="clear: both;"></div>
    <div id="includedFooter"></div>
</body>
</html>