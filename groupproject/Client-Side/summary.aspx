﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="summary.aspx.cs" Inherits="GroupProject.Client_Side.summary" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
    <script src="../Scripts/jquery-2.2.1.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#includedHeader").load("header.aspx");
            $("#includedFooter").load("footer.aspx");
        });
    </script>
</head>
<body>
    <div id="includedHeader"></div>
    <div style="clear:both;"></div>
    <div id="contents">
        <div id="left_cont">
            <p id="event_title">Booking ID: 84927856</p>
            <div class="event_border"></div>
            <img id="event_details_img" src="../Images/tmp_img.jpg" />
            <div id="event_details">
                <p class="details_text">
                    <b>Justin Bieber World Tour</b><br />
                    Brixton Academy, 211 Stockwell R, London, SW9 9SL<br />
                    <b>Wednesday 5th October 2016</b> 19:00<br /><br />
                    Number of tickets Brought: 4
                </p>
            </div>
            <div class="event_border"></div>
            <table class="summary" id="tickets_table">
                <tr>
                    <td><p>type</p></td>
                    <td><p>number</p></td>
                    <td><p>price</p></td>
                </tr>
                <tr>
                    <td><p>type</p></td>
                    <td><p>number</p></td>
                    <td><p>price</p></td>
                </tr>
                <tr>
                    <td></td>
                    <td><p><b>Total Price:</b></p></td>
                    <td><p><b>£100</b></p></td>
                </tr>
            </table>
        </div>
        <div id="right_cont">
            <div id="social_media">
                <p class="header">Social Media</p>
                <a href="#"><img src="../Images/F_icon.png" /></a>
                <a href="#"><img src="../Images/Google+_Icon.png" /></a>
                <a href="#"><img src="../Images/instagram-xxl.png" /></a>
                <a href="#"><img src="../Images/Twitter-icon.png" /></a>
                <a href="#"><img src="../Images/Youtube_Icon.png" /></a>
                <a href="#"><img src="../Images/square-tumblr-512.png" /></a>
            </div>

            <div id="testimonials_main_cont">
                <p class="header">Testimonials</p>
                <div id="first_testimonial" class="testimonial_sub_cont">
                    <p class="sub_header">Test</p>
                    <p>Tmp data</p>
                    <!-- raiting system -->
                    <p class="name">George</p>
                </div>
                <div class="testimonial_sub_cont">
                    <p class="sub_header">Test</p>
                    <p>Tmp data</p>
                    <!-- raiting system -->
                    <p class="name">George</p>
                </div>
            </div>

            <div id="payment_cont">
                <p class="header">Payment Methods</p>
                <img src="../Images/payment.jpg" />
            </div>
        </div>
    </div>
    <div style="clear:both;"></div>
    <div id="includedFooter"></div>
</body>
</html>
