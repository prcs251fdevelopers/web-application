﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Diagnostics;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace GroupProject.Client_Side
{
    public partial class index : System.Web.UI.Page
    {
        public class Event
        {
            [JsonProperty("EVENTID")]
            public string eventID { get; set; }
            [JsonProperty("COMPANYID")]
            double companyID { get; set; }
            [JsonProperty("VENUEID")]
            double venueID { get; set; }
            [JsonProperty("EVENTNAME")]
            public string eventName { get; set; }
            [JsonProperty("EVENTTYPE")]
            string eventType { get; set; }
            [JsonProperty("STARTTIME")]
            string startTime { get; set; }
            [JsonProperty("ENDTIME")]
            string endTime { get; set; }
            [JsonProperty("SEATINGPRICE")]
            string seatingPrice { get; set; }
            [JsonProperty("STANDINGPRICE")]
            string standingPrice { get; set; }
            [JsonProperty("AGERESTRICTION")]
            string ageRestriction { get; set; }
        }

        protected void Page_Load(object sender, EventArgs e)
        {
            //APIController.Call_Events();

            WebRequest request = WebRequest.Create("http://xserve.uopnet.plymouth.ac.uk/modules/intproj/prcs251f/api/events");

            WebResponse response = request.GetResponse();

            Debug.WriteLine(((HttpWebResponse)response).StatusDescription);

            Stream dataStream = response.GetResponseStream();

            StreamReader reader = new StreamReader(dataStream);

            String responseFromServer = reader.ReadToEnd();

            Get_FormattedData(responseFromServer);

            Debug.WriteLine(responseFromServer);

            //test.Text = responseFromServer;

            reader.Close();

            response.Close();
        }

        protected void Get_FormattedData(String responseFromServer)
        {
            //string[] keyData;
            JavaScriptSerializer parser = new JavaScriptSerializer();

            //ArrayOfCOMPANy?
            if (responseFromServer.Contains("Event"))
            {
                //var data = JsonConvert.DeserializeObject<List<Event>>(responseFromServer);
                var data = parser.Deserialize<List<Event>>(responseFromServer);

                foreach (var item in data)
                {
                    test.Text += item.eventID;
                    //Console.WriteLine("eventid " + item.eventID);
                }
            }
            /*if (responseFromServer.Contains("Guest"))
            {
                var data = parser.Deserialize<Guests>(responseFromServer);
            }
            if (responseFromServer.Contains("Venue"))
            {
                var data = parser.Deserialize<Venues>(responseFromServer);
            }
            if (responseFromServer.Contains("Company"))
            {
                var data = parser.Deserialize<Companies>(responseFromServer);
            }
            if (responseFromServer.Contains("Customer"))
            {
                var data = parser.Deserialize<Customers>(responseFromServer);
            }
            if (responseFromServer.Contains("Ticket"))
            {
                var data = parser.Deserialize<Tickets>(responseFromServer);
            }
            if (responseFromServer.Contains("User"))
            {
                var data = parser.Deserialize<Users>(responseFromServer);
            }*/
        }

        protected bool DisplayEvents(Event data)
        {
            return true;
        }
    }
}
