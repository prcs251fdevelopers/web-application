﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="category.aspx.cs" Inherits="GroupProject.Client_Side.category" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title>Events Booking System</title>
    <script src="../Scripts/jquery-2.2.1.min.js"></script>
    <script type="text/javascript">
        $(function () {
            $("#includedHeader").load("header.aspx");
            $("#includedFooter").load("footer.aspx");
        });
    </script>
</head>
<body>
    <div id="includedHeader"></div>
        <div style="clear:both;"></div>
    <div id="contents">
        <div id="left_cont">
            <div class="sub_menu">
                <a href="#">
                    <img src="../Images/tmp_img.jpg" />
                    <span class="sub_menu_span">sub cat 1</span>
                </a>
            </div>
            <div class="sub_menu">
                <a href="#">
                    <img src="../Images/tmp_img.jpg" />
                    <span class="sub_menu_span">sub cat 2</span>
                </a>
            </div>
            <div class="sub_menu">
                <a href="#">
                    <img src="../Images/tmp_img.jpg" />
                    <span class="sub_menu_span">sub cat 3</span>
                </a>
            </div>
            <div class="sub_menu">
                <a href="#">
                    <img src="../Images/tmp_img.jpg" />
                    <span class="sub_menu_span">sub cat 4</span>
                </a>
            </div>
            <div class="sub_menu">
                <a href="#">
                    <img src="../Images/tmp_img.jpg" />
                    <span class="sub_menu_span">sub cat 5</span>
                </a>
            </div>
            <div class="sub_menu">
                <a href="#">
                    <img src="../Images/tmp_img.jpg" />
                    <span class="sub_menu_span">sub cat 6</span>
                </a>
            </div>
        </div>
        <div id="right_cont">
            <div id="social_media">
                <p class="header">Social Media</p>
                <a href="#"><img src="../Images/F_icon.png" /></a>
                <a href="#"><img src="../Images/Google+_Icon.png" /></a>
                <a href="#"><img src="../Images/instagram-xxl.png" /></a>
                <a href="#"><img src="../Images/Twitter-icon.png" /></a>
                <a href="#"><img src="../Images/Youtube_Icon.png" /></a>
                <a href="#"><img src="../Images/square-tumblr-512.png" /></a>
            </div>

            <div id="testimonials_main_cont">
                <p class="header">Testimonials</p>
                <div id="first_testimonial" class="testimonial_sub_cont">
                    <p class="sub_header">Test</p>
                    <p>Tmp data</p>
                    <!-- raiting system -->
                    <p class="name">George</p>
                </div>
                <div class="testimonial_sub_cont">
                    <p class="sub_header">Test</p>
                    <p>Tmp data</p>
                    <!-- raiting system -->
                    <p class="name">George</p>
                </div>
            </div>

            <div id="payment_cont">
                <p class="header">Payment Methods</p>
                <img src="../Images/payment.jpg" />
            </div>
        </div>
    </div>
        <div style="clear:both;"></div>
    <div id="includedFooter"></div>
</body>
</html>
