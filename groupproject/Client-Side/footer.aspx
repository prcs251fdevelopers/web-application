﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="footer.aspx.cs" Inherits="GroupProject.Client_Side.footer" %>

<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <title></title>
</head>
<body>
    <footer>
        <div id="footer_cont">
            <div id="footer_section1">
                <p class="footer_header">Navigation</p>
                <ul>
                    <li><a href="index.html">Home</a></li>
                    <li><a href="#">Music</a></li>
                    <li><a href="#">Sport</a></li>
                    <li><a href="#">Comedy</a></li>
                    <li><a href="#">theatre</a></li>
                </ul>
                   
            </div>
            <div id="footer_section2">
                <p class="footer_header">Contact Us</p>
                <p>
                    Phone: XXXXXXXXXX<br />
                    Email: <a href="#">info@eventssystem.com</a>
                </p>
            </div>
            <div style="clear:both;"></div>
        </div>
    </footer>
</body>
</html>
